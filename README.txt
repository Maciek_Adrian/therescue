"Rescue" is a first-person platform game created for the Windows platform. 
Integration with the device measuring the biophysiological data makes the game stand out from other games.

The player takes the role of a lifeguard remotely monitoring work in the mine.
In the first moments of the game, one of the tiers of the mine collapses.
The player logs in to the mechanical unit placed at this level.
By steering it, he will have to reach imprisoned miners and lead them out of the endangered place.

The following version of "The Rescue" contains the game without biophysiological data collection.
For the full version of the game please contact me by mail maciek.h.adrian@gmail.com.

